package com.example.RecentReport;

import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.Page.RecentReport;
import com.example.demo.Login;
import com.example.demo.MainActivity;
import com.example.demo.R;
import com.jeremyfeinstein.slidingmenu.lib.app.SlidingActivity;

public class RecentDetail extends Fragment {

    public static final String KEY_NAME = "name";
    public static final String TAG = "com.example.RecentReport.RecentDetail";

    TextView myName;
    private MainActivity parent;

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.recent_detail, container, false);

        myName = (TextView) v.findViewById(R.id.name);

        parent = (MainActivity) getActivity();

        parent.setActionBarLeft(R.drawable.nav_menu, MainActivity.ID_TEXT_NULL);
        parent.setActionBarRight(0, R.string.send);
        parent.setTitle(R.string.share_my_buzz);

        SharedPreferences shareData = getActivity().getApplication().getSharedPreferences(Login.APP_NAME, Context.MODE_PRIVATE);
        String name = shareData.getString(KEY_NAME, null);
        myName.setText(name);

//        btn_left.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//            }
//        });
       return v;
    }
}
