package com.example.Page;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.example.demo.R;
import com.example.notificationsMain.AdapterNotifi;
import com.example.notificationsMain.ItemNotifi;

import java.util.ArrayList;

public class NotificationsMain extends Fragment {

    public static final String TAG = "com.example.Page.NotificationsMain";
    private ListView listview;
    public AdapterNotifi mAdapter;

    ArrayList<ItemNotifi> mArrayList;
    TextView text_name;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.notifications_main, container, false);

       // text_name = (TextView) v.findViewById(R.id.text_name);
        listview = (ListView) v.findViewById(R.id.listNotifications);

      //  text_name.setText(R.string.notifications);

        getView();

        return v;
    }

    public View getView() {
        mArrayList = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            ItemNotifi data1 = new ItemNotifi("here", 68);
            ItemNotifi data2 = new ItemNotifi("here", 10);
            mArrayList.add(data2);
            mArrayList.add(data1);
        }

        mAdapter = new AdapterNotifi(getActivity(), R.layout.item_list_notifi, mArrayList);
        listview.setAdapter(mAdapter);
        return null;
    }


}
