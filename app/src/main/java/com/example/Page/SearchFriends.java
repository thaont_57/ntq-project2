package com.example.Page;

import android.app.Fragment;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.AdapterListGrid.GridChat;
import com.example.AdapterListGrid.ListChat;
import com.example.SettingFriend.SearchSetting;
import com.example.demo.MainActivity;
import com.example.demo.R;

public class SearchFriends extends Fragment {

    public  static final String TAG = "com.example.Page.SearchFriends";

    ImageButton view;
    TextView searchSetting;
    private static boolean listchat = true;
    TextView textButton, name_action;
    Fragment list = new ListChat();
    Fragment grid = new GridChat();
    SearchSetting search = new SearchSetting();
    View v;
    private MainActivity parent;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.search_friends, container, false);

        view = (ImageButton) v.findViewById(R.id.button_view);
        searchSetting = (TextView) v.findViewById(R.id.search);
        textButton = (TextView) v.findViewById(R.id.text_button);

        parent = (MainActivity) getActivity();

        parent.setTitle(R.string.meet_people);
        parent.setActionBarRight(0, R.string.search);
        parent.setActionBarLeft(0, R.string.common_cancel);

        view.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (listchat) {
                    startListChat();

                } else {
                    startGridChat();

                }
            }
        });

        searchSetting.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                getFragmentManager().beginTransaction().replace(R.id.frame, search, SearchSetting.TAG).addToBackStack(null).commit();
            }
        });

        return v;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState == null) {
            getChildFragmentManager().beginTransaction().replace(R.id.frame, search, SearchSetting.TAG).addToBackStack(null).commit();
        }

    }

    private void startGridChat() {
        //v.getContext().startActivity(new Intent(v.getContext(), GridChat.class));
        getChildFragmentManager().beginTransaction().replace(R.id.frame, grid, GridChat.TAG).commit();
        view.setBackgroundResource(R.drawable.ic_pannel_list);
        textButton.setText("List View");
        listchat = true;
    }

    private void startListChat() {
        //v.getContext().startActivity(new Intent(v.getContext(), ListChat.class));
        getChildFragmentManager().beginTransaction().replace(R.id.frame, list, ListChat.TAG).commit();
        view.setBackgroundResource(R.drawable.ic_pannel_grid);
        textButton.setText("Grid View");
        listchat = false;
    }



}
