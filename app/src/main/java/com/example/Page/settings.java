package com.example.Page;

import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TextView;
import android.widget.Toast;

import com.example.Setting.ChangePassword;
import com.example.Setting.Deactive;
import com.example.Setting.Distance;
import com.example.Setting.Notifications;
import com.example.Setting.Term;
import com.example.demo.Login;
import com.example.demo.R;

public class settings extends Fragment implements OnClickListener, OnCheckedChangeListener, OnSharedPreferenceChangeListener {

    public static final String KEY_SOUND = "sound";
    public static final String KEY_VIB = "vib";
    public static final String KEY_DISTANCE = "distance";
    public static final String TAG = "com.example.Page.settings";

    TextView distanceText;
    public SharedPreferences shareData;
    public SharedPreferences.Editor editor;
    CheckBox soundOn;
    CheckBox vibOn;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.settings);

        shareData = getActivity().getApplication().getSharedPreferences(Login.APP_NAME, Context.MODE_PRIVATE);
        editor = shareData.edit();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.settings, container, false);

        distanceText = (TextView) v.findViewById(R.id.distance_text);
        v.findViewById(R.id.notifications).setOnClickListener(this);
        v.findViewById(R.id.change_password).setOnClickListener(this);
        v.findViewById(R.id.deactive).setOnClickListener(this);
        v.findViewById(R.id.term).setOnClickListener(this);
        v.findViewById(R.id.distance).setOnClickListener(this);
        soundOn = (CheckBox) v.findViewById(R.id.soundOn);
        vibOn = (CheckBox) v.findViewById(R.id.vibOn);
        soundOn.setOnCheckedChangeListener(this);
        vibOn.setOnCheckedChangeListener(this);



        return v;
    }

    private void load() {
        boolean soundState = shareData.getBoolean(KEY_SOUND, false);
        boolean vibState = shareData.getBoolean(KEY_VIB, false);
        String distance = shareData.getString(KEY_DISTANCE, "");

        soundOn.setChecked(soundState);
        vibOn.setChecked(vibState);
        distanceText.setText(distance);

        //	Toast.makeText(getApplicationContext(), distance, Toast.LENGTH_SHORT).show();

    }


    @Override
    public void onStart() {
        load();
        editor.putString(KEY_DISTANCE, distanceText.getText().toString()).commit();
        Toast.makeText(getActivity(), distanceText.getText().toString(), Toast.LENGTH_SHORT).show();
        super.onStart();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.notifications:
                Notifications notifications = new Notifications();
                getFragmentManager().beginTransaction().replace(R.id.frame_main, notifications, Notifications.TAG).addToBackStack(null).commit();
                break;
            case R.id.deactive:
                Deactive deactive = new Deactive();
                getFragmentManager().beginTransaction().replace(R.id.frame_main, deactive, Deactive.TAG).addToBackStack(null).commit();
                break;
            case R.id.change_password:
                ChangePassword changePassword = new ChangePassword();
                getFragmentManager().beginTransaction().replace(R.id.frame_main, changePassword, ChangePassword.TAG).addToBackStack(null).commit();
                break;
            case R.id.term:
                Term term = new Term();
                getFragmentManager().beginTransaction().replace(R.id.frame_main, term, Term.TAG).addToBackStack(null).commit();
                break;
            case R.id.distance:
                Distance distance = new Distance();
                getFragmentManager().beginTransaction().replace(R.id.frame_main, distance, Distance.TAG).addToBackStack(null).commit();
                break;
            default:
                break;
        }

    }


    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        switch (buttonView.getId()) {
            case R.id.soundOn:
                editor.putBoolean(KEY_SOUND, soundOn.isChecked());
                Toast.makeText(getActivity(), soundOn.isChecked() + "", Toast.LENGTH_SHORT).show();
                editor.commit();
                break;
            case R.id.vibOn:
                editor.putBoolean(KEY_VIB, vibOn.isChecked());
                Toast.makeText(getActivity(), vibOn.isChecked() + "", Toast.LENGTH_SHORT).show();
                editor.commit();
                break;

            default:
                break;
        }

    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        load();

    }


}
