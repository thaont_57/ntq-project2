package com.example.Page;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.chats.chatsDeleteFragment;
import com.example.chats.chatsFragment;
import com.example.demo.R;

public class Chats extends Fragment implements OnClickListener {

    public static final String TAG = "com.example.Page.Chats";

    final Fragment chats = new chatsFragment();
    final Fragment deleteChats = new chatsDeleteFragment();
    TextView btnLeft, btnCenter, btnRight;
    boolean check = true;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.chats, container, false);

//        btnRight = (TextView) v.findViewById(R.id.btn_right);
//        btnLeft = (TextView) v.findViewById(R.id.btn_left);
//        btnCenter = (TextView) v.findViewById(R.id.btn_center);

        btnRight.setOnClickListener(this);

//        if (savedInstanceState == null) {
//            getFragmentManager().beginTransaction().replace(R.id.frame_chats, chats, chatsFragment.TAG).commit();
//            check = false;
//        }

        return v;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_right:
                if (check) {
                    done();
                    check = false;
                } else {
                    delete();
                    check = true;
                }
                break;
            default:
                break;
        }
    }

    private void done() {
  //      getFragmentManager().beginTransaction().replace(R.id.frame_chats, chats, chatsFragment.TAG).commit();
        btnLeft.setText(R.string.shake_to_chat_title);
        btnLeft.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_menu_shaketochat, 0, 0);
        btnCenter.setText(R.string.add_friends_title);
        btnCenter.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_menu_add_friends, 0, 0);
        btnRight.setText(R.string.sliding_menu_right_top_txt_edit);
        btnRight.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_menu_edit, 0, 0);

    }


    private void delete() {
   //     getFragmentManager().beginTransaction().replace(R.id.frame_chats, deleteChats, chatsDeleteFragment.TAG).commit();
        btnLeft.setText(R.string.sliding_menu_right_top_txt_mark_readed);
        btnLeft.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_menu_mark_readed, 0, 0);
        btnCenter.setText(R.string.sliding_menu_right_top_txt_delete_all);
        btnCenter.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_menu_delete_all, 0, 0);
        btnRight.setText(R.string.sliding_menu_right_top_txt_done);
        btnRight.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_menu_edit, 0, 0);
    }

}
