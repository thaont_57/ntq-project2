package com.example.Page;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.Gifts.FragmentGift;
import com.example.demo.MainActivity;
import com.example.demo.R;

public class Gifts extends Fragment {
    public  static  final String TAG = "com.example.Page.Gifts";

    public static TextView text_name;
    private MainActivity parent;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.give_gifts, container, false);

        parent = (MainActivity) getActivity();

        parent.setTitle(R.string.title_textview_my_profile_give_gift);
        parent.setActionBarRight(MainActivity.ID_IMAGE_NAV_RIGHT, MainActivity.ID_TEXT_NULL);
        parent.setActionBarLeft(R.drawable.nav_btn_back, MainActivity.ID_TEXT_NULL);

//        btn.setOnClickListener(new OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//                getChildFragmentManager().popBackStack();
//            }
//        });

        final FragmentGift gift = new FragmentGift();
        getChildFragmentManager().beginTransaction().add(R.id.frame_gift, gift, FragmentGift.TAG).commit();

        return v;
    }
}
