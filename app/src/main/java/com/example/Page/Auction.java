package com.example.Page;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.Auction.ListAuction;
import com.example.demo.MainActivity;
import com.example.demo.R;

public class Auction extends Fragment {

    public static final String TAG = "com.example.Page.Auction";
    private MainActivity parent;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
       // setContentView(R.layout.auction);

        Fragment listAuction = new ListAuction();
        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction().replace(R.id.frame, listAuction, ListAuction.TAG).commit();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.auction, container, false);

        parent = (MainActivity) getActivity();

        parent.setTitle(R.string.look_at_me);
        parent.setActionBarLeft(R.drawable.nav_menu, MainActivity.ID_TEXT_NULL);
        parent.setActionBarRight(MainActivity.ID_IMAGE_NAV_RIGHT, MainActivity.ID_TEXT_NULL);

        return v;
    }
}
