package com.example.Page;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.DrawerLayout;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ScrollView;

import com.example.RecentReport.RecentDetail;
import com.example.demo.R;

public class RecentReport extends FragmentActivity implements OnClickListener {

    public static final String TAG = "com.example.demo.RecentReport";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.recent_report);

        findViewById(R.id.searchs_friends).setOnClickListener(this);
        findViewById(R.id.ckeck_timeline).setOnClickListener(this);
        findViewById(R.id.Auction).setOnClickListener(this);
        findViewById(R.id.shaketochat).setOnClickListener(this);
        findViewById(R.id.profile).setOnClickListener(this);
        findViewById(R.id.chats).setOnClickListener(this);
        findViewById(R.id.points).setOnClickListener(this);
        findViewById(R.id.settings).setOnClickListener(this);
        findViewById(R.id.notifications_main).setOnClickListener(this);
        findViewById(R.id.btn_recent_report).setOnClickListener(this);
        findViewById(R.id.gift).setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.points:
                Points points = new Points();
                getFragmentManager().beginTransaction().replace(R.id.frame_main, points, Points.TAG).addToBackStack(null).commit();
                break;
            case R.id.searchs_friends:
                SearchFriends searchFriends = new SearchFriends();
                getFragmentManager().beginTransaction().replace(R.id.frame_main, searchFriends, SearchFriends.TAG).addToBackStack(null).commit();
                break;
            case R.id.profile:
                EditProfile editProfile = new EditProfile();
                getFragmentManager().beginTransaction().replace(R.id.frame_main, editProfile, EditProfile.TAG).addToBackStack(null).commit();
                break;
            case R.id.btn_recent_report:
                RecentDetail recentdetail = new RecentDetail();
                getFragmentManager().beginTransaction().replace(R.id.frame_main, recentdetail, RecentDetail.TAG).addToBackStack(null).commit();
                break;
            case R.id.settings:
                settings fsetting = new settings();
                getFragmentManager().beginTransaction().replace(R.id.frame_main, fsetting, settings.TAG).addToBackStack(null).commit();
                break;
            case R.id.chats:
                Chats chats = new Chats();
                getFragmentManager().beginTransaction().replace(R.id.frame_main, chats, Chats.TAG).addToBackStack(null).commit();
                break;
            case R.id.notifications_main:
                NotificationsMain notificationsMain = new NotificationsMain();
                getFragmentManager().beginTransaction().replace(R.id.frame_main, notificationsMain, NotificationsMain.TAG).addToBackStack(null).commit();
                break;
            case R.id.gift:
                Gifts gifts = new Gifts();
                getFragmentManager().beginTransaction().replace(R.id.frame_main, gifts, Gifts.TAG).addToBackStack(null).commit();
                break;
            case R.id.Auction:
                Auction auction = new Auction();
                getFragmentManager().beginTransaction().replace(R.id.frame_main, auction, Auction.TAG).addToBackStack(null).commit();
            default:
                break;
        }
    }

}
