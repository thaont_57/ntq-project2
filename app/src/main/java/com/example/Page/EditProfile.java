package com.example.Page;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.NumberPicker;
import android.widget.NumberPicker.OnValueChangeListener;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.example.RecentReport.RecentDetail;
import com.example.demo.Login;
import com.example.demo.MainActivity;
import com.example.demo.R;
import com.example.myinterface.onClickListenActionBar;

public class EditProfile extends Fragment
        implements OnClickListener, OnSharedPreferenceChangeListener, OnValueChangeListener {

    private static final String DEFAULT = "N/A";
    public static final String KEY_GENDER = "gender";
    public static final String KEY_AGE = "age";
    public static final String KEY_RELATIONSHIP = "relationship";
    public static final String TAG = "com.example.Page.EditProfile";

    private MainActivity parent;

    String[] myRelationship, gender;
    SharedPreferences shareData;
    TextView Name, Gender, Age, Relationship;
    LinearLayout edit_age;
    ImageView edit_gender, edit_name;
    TableRow myListview;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        shareData = getActivity().getApplication().getSharedPreferences(Login.APP_NAME, Context.MODE_PRIVATE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.edit_profile, container, false);

        parent = (MainActivity) getActivity();

        myListview = (TableRow) v.findViewById(R.id.relationship);
        Name = (TextView) v.findViewById(R.id.name_text);
        Gender = (TextView) v.findViewById(R.id.gender_text);
        Age = (TextView) v.findViewById(R.id.age);
        Relationship = (TextView) v.findViewById(R.id.relationship_new);
        edit_age = (LinearLayout) v.findViewById(R.id.edit_age);
        edit_gender = (ImageView) v.findViewById(R.id.edit_gender);
        edit_name = (ImageView) v.findViewById(R.id.edit_name);

        parent.setActionBarLeft(0, R.string.common_cancel);
        parent.setTitle(R.string.edit_my_profile_title);
        parent.setActionBarRight(0, R.string.save_dialog_title);

        parent.listener = new onClickListenActionBar() {
            @Override
            public void onClickActionBarLeft() {
                load();
                getFragmentManager().popBackStack();
                Toast.makeText(getActivity(), "CANCEL", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onClickActionBarRight() {
                // shareData.registerOnSharedPreferenceChangeListener(new );
                save();
                load();
                Toast.makeText(getActivity(), "SAVE", Toast.LENGTH_SHORT).show();

            }
        };

        edit_age.setOnClickListener(this);
        edit_gender.setOnClickListener(this);
        edit_name.setOnClickListener(this);
        // actionRight.setOnClickListener(this);
        myListview.setOnClickListener(this);
        //  actionLeft.setOnClickListener(this);

        load();

        return v;
    }

    private void load() {
        String name = shareData.getString(RecentDetail.KEY_NAME, DEFAULT);
        String gender = shareData.getString(KEY_GENDER, DEFAULT);
        String age = shareData.getString(KEY_AGE, DEFAULT);
        String relationship = shareData.getString(KEY_RELATIONSHIP, "ask me");
        if (name.equals(DEFAULT)) {
            Toast.makeText(getActivity(), "Data not save!", Toast.LENGTH_SHORT).show();
        } else {
            //
            Name.setText(name);
            Gender.setText(gender);
            Age.setText(age);
            Relationship.setText(relationship);
        }
    }

    private void save() {
        SharedPreferences.Editor edittor = shareData.edit();
        edittor.putString(RecentDetail.KEY_NAME, Name.getText().toString());
        edittor.putString(KEY_GENDER, Gender.getText().toString());
        edittor.putString(KEY_AGE, Age.getText().toString());
        edittor.putString(KEY_RELATIONSHIP, Relationship.getText().toString());
        edittor.commit();

    }

    public void show() {
        final Dialog d = new Dialog(getActivity());
        d.setTitle("Relationship");
        //d.requestWindowFeature(Window.FEATURE_NO_TITLE);
        d.setContentView(R.layout.list_relationship);
        ListView myLst = (ListView) d.findViewById(R.id.list_relationship);
        myLst.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        myLst.setTextFilterEnabled(true);
        myRelationship = getResources().getStringArray(R.array.relationship);
        myLst.setAdapter(
                new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_single_choice, myRelationship));
        myLst.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String getRelationship = myRelationship[position];
                Relationship.setText(getRelationship);
                d.dismiss();

            }
        });
        d.show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.edit_name:
                dialogEditName();
                break;
            case R.id.edit_age:
                dialogEditAge();
                break;
            case R.id.edit_gender:
                dialogEditGender();
                break;
//            case R.id.action_right:
//                shareData.registerOnSharedPreferenceChangeListener(this);
//                save();
//                load();
//                Toast.makeText(getActivity(), "SAVE", Toast.LENGTH_SHORT).show();
//                break;
            case R.id.relationship:
                show();
                break;
//            case R.id.action_left:
//                load();
//                getFragmentManager().popBackStack();
//                Toast.makeText(getActivity(), "CANCEL", Toast.LENGTH_SHORT).show();
//                break;

            default:
                break;
        }
    }

    private void dialogEditName() {

        AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
        dialog.setTitle("Edit name");
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View alertDialogView = inflater.inflate(R.layout.dialog_edit_profile, null);
        dialog.setView(alertDialogView);

        dialog.setNegativeButton("OK", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                EditText editText = (EditText) alertDialogView.findViewById(R.id.edit_profile);
                String getName = editText.getText().toString();
                Name.setText(getName);
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void dialogEditGender() {
        //	String genderString = shareData.getString("new gender", DEFAULT);
        final AlertDialog dialog = new AlertDialog.Builder(getActivity()).create();
        dialog.setTitle("Edit gender");
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View alertDialogView = inflater.inflate(R.layout.dialog_edit_gender, null);
        dialog.setView(alertDialogView);
        ListView myLst = (ListView) alertDialogView.findViewById(R.id.listgender);
        myLst.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        myLst.setTextFilterEnabled(true);
        gender = getResources().getStringArray(R.array.gender);
        ArrayAdapter<String> item = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1,
                gender);
        myLst.setAdapter(item);
        myLst.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String itemList = gender[position];
                Gender.setText(itemList);
                dialog.dismiss();
            }

        });

        dialog.show();
    }

    private void dialogEditAge() {

        AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
        dialog.setTitle("Edit age");
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View alertDialogView = inflater.inflate(R.layout.numberpicker_age, null);
        dialog.setView(alertDialogView);
        final NumberPicker numberPicker = (NumberPicker) alertDialogView.findViewById(R.id.numberPickerAge);
        numberPicker.setMaxValue(120);
        numberPicker.setMinValue(18);
        numberPicker.setWrapSelectorWheel(false);
        numberPicker.setOnValueChangedListener(this);
        dialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                String getAge = numberPicker.getValue() + "";
                Age.setText(getAge);
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        load();
    }

    @Override
    public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
        // TODO Auto-generated method stub

    }
}
