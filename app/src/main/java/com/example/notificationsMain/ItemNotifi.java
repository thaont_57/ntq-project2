package com.example.notificationsMain;

public class ItemNotifi {
    String Distance;
    int Time;

    public ItemNotifi(String distance, int time) {
        Distance = distance;
        Time = time;
    }

    public String getDistance() {
        return Distance;
    }

    public int getTime() {
        return Time;
    }

}
