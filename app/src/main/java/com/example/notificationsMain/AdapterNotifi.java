package com.example.notificationsMain;

import java.util.ArrayList;

import com.example.demo.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class AdapterNotifi extends ArrayAdapter<ItemNotifi> {
    LayoutInflater vi;
    int Resource;
    ArrayList<ItemNotifi> itemList;
    ViewHolder holder;

    public AdapterNotifi(Context context, int textViewResourceId, ArrayList<ItemNotifi> objects) {
        super(context, textViewResourceId, objects);
        vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        Resource = textViewResourceId;
        itemList = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            holder = new ViewHolder();
            v = vi.inflate(Resource, null);
            holder.distance = (TextView) v.findViewById(R.id.distance_notifi);
            holder.time = (TextView) v.findViewById(R.id.time_notifi);
            v.setTag(holder);
        } else {
            holder = (ViewHolder) v.getTag();
        }
        holder.distance.setText(itemList.get(position).getDistance() + "");
        holder.time.setText(itemList.get(position).getTime() + "");
        return v;
    }

    static class ViewHolder {
        TextView distance;
        TextView time;

    }


}
