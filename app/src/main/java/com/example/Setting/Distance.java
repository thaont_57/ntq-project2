package com.example.Setting;

import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;

import com.example.Page.settings;
import com.example.demo.Login;
import com.example.demo.R;

public class Distance extends Fragment implements OnSharedPreferenceChangeListener {

    public static final String TAG = "com.example.Setting.Distance";

    SharedPreferences shareData;
    SharedPreferences.Editor editor;
    RadioButton miles;
    RadioButton kilometers;
    RadioGroup myGroupRadio;
    ImageButton back;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.distance);

        shareData = getActivity().getApplication().getSharedPreferences(Login.APP_NAME, Context.MODE_PRIVATE);
        editor = shareData.edit();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.distance, container, false);

        myGroupRadio = (RadioGroup) v.findViewById(R.id.distance_radio);
        back = (ImageButton) v.findViewById(R.id.action_left);
        miles = (RadioButton) v.findViewById(R.id.miles);
        kilometers = (RadioButton) v.findViewById(R.id.kilometers);

        load();

        back.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStack();

            }
        });
        myGroupRadio.setOnCheckedChangeListener(new OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.miles:
                        editor.putString(settings.KEY_DISTANCE, miles.getText().toString()).commit();
                        break;
                    case R.id.kilometers:
                        editor.putString(settings.KEY_DISTANCE, kilometers.getText().toString()).commit();
                        break;
                }

            }
        });

        return v;
    }

    private void load() {
        String distance = shareData.getString(settings.KEY_DISTANCE, "");
        if (distance.equals("Miles")) {
            miles.setChecked(true);
            kilometers.setChecked(false);
        } else {
            kilometers.setChecked(true);
            miles.setChecked(false);
        }
    }

    @Override
    public void onStart() {
        load();
        super.onStart();
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        load();

    }

}
