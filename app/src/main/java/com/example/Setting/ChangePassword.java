package com.example.Setting;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.demo.Login;
import com.example.demo.MainActivity;
import com.example.demo.R;

public class ChangePassword extends Fragment {

    public static final String TAG = "com.example.Setting.ChangePassword";

    EditText enterOldPass, enterNewPass, enterConfirmPass;
    SharedPreferences shareData;
    SharedPreferences.Editor editor;
    TextView changePass;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.changepassword, container, false);

        changePass = (TextView) v.findViewById(R.id.changePass);
        enterOldPass = (EditText) v.findViewById(R.id.oldPass);
        enterNewPass = (EditText) v.findViewById(R.id.newPass);
        enterConfirmPass = (EditText) v.findViewById(R.id.confirmPass);

        MainActivity parent = (MainActivity) getActivity();

        parent.setTitle(R.string.login_forgot_change_password);
        parent.setActionBarLeft(R.drawable.nav_btn_back, MainActivity.ID_TEXT_NULL);
        parent.setActionBarRight(0, MainActivity.ID_TEXT_NULL);

        shareData = getActivity().getApplication().getSharedPreferences(Login.APP_NAME, Context.MODE_PRIVATE);

//        back.setOnClickListener(new OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//                getFragmentManager().popBackStack();
//            }
//        });

        changePass.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (checkOldPass() && checkTwoPass()) {
                    editor = shareData.edit();
                    editor.putString(Login.KEY_PASSWORD, enterNewPass.getText().toString()).commit();
                    AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
                    dialog.setTitle("Change password successfull!");
                    dialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            enterConfirmPass.setText("");
                            enterNewPass.setText("");
                            enterOldPass.setText("");
                            dialog.dismiss();
                        }
                    });
                    dialog.show();

                } else
                    myDialog();
            }
        });

        return v;
    }

    private boolean checkOldPass() {
        String oldPass = shareData.getString(Login.KEY_PASSWORD, "");
        String OldpassEnter = enterOldPass.getText().toString();
        if (OldpassEnter.equals(oldPass)) {
            Toast.makeText(getActivity().getApplicationContext(), "true", Toast.LENGTH_SHORT).show();
            return true;
        } else {
            Toast.makeText(getActivity().getApplicationContext(), "false", Toast.LENGTH_SHORT).show();
            return false;
        }

    }

    private boolean checkTwoPass() {
        return enterNewPass.getText().toString().equals(enterConfirmPass.getText().toString());
    }

    private void myDialog() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
        dialog.setTitle("Error change password!");
        dialog.setMessage("Some information are wrong. Please enter again!");
        dialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                enterConfirmPass.setText("");
                enterNewPass.setText("");
                enterOldPass.setText("");
                dialog.dismiss();
            }
        });
        dialog.show();
    }


}
