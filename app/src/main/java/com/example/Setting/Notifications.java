package com.example.Setting;

import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.demo.Login;
import com.example.demo.R;

public class Notifications extends Fragment implements OnCheckedChangeListener {

    public static final String KEY_UPDATE = "update";
    public static final String KEY_ANDG = "andG";
    public static final String KEY_CHECKMEOUT = "checkme out";
    public static final String TAG = "com.example.Setting";

    public SharedPreferences shareData;
    public SharedPreferences.Editor editor;
    CheckBox update, andG, checkmeOut;
    TextView actionCenter, actionRight;
    ImageView back;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        // setContentView(R.layout.notifications);



        shareData = getActivity().getApplication().getSharedPreferences(Login.APP_NAME, Context.MODE_PRIVATE);
        editor = shareData.edit();


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.notifications, container, false);

//        actionCenter = (TextView) v.findViewById(R.id.action_center);
//        actionRight = (TextView) v.findViewById(R.id.action_right);
        update = (CheckBox) v.findViewById(R.id.updates);
        andG = (CheckBox) v.findViewById(R.id.andG);
        checkmeOut = (CheckBox) v.findViewById(R.id.checkMe);
//        back = (ImageView) v.findViewById(R.id.action_left);

//        actionCenter.setText(R.string.notification_title);
//        actionRight.setText(R.string.save_dialog_title);


        update.setOnCheckedChangeListener(this);

        andG.setOnCheckedChangeListener(this);

        checkmeOut.setOnCheckedChangeListener(this);

        back.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStack();

            }
        });

        actionRight.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                save();
                load();
                Toast.makeText(getActivity(), "SAVE", Toast.LENGTH_SHORT).show();
            }


        });

        load();

        return v;
    }

    private void save() {
        editor.putBoolean(KEY_UPDATE, update.isChecked()).commit();
        editor.putBoolean(KEY_ANDG, andG.isChecked()).commit();
        editor.putBoolean(KEY_CHECKMEOUT, checkmeOut.isChecked()).commit();
    }

    private void load() {
        boolean updateState = shareData.getBoolean(KEY_UPDATE, false);
        boolean andGState = shareData.getBoolean(KEY_ANDG, false);
        boolean checkMeState = shareData.getBoolean(KEY_CHECKMEOUT, false);

        update.setChecked(updateState);
        andG.setChecked(andGState);
        checkmeOut.setChecked(checkMeState);

    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        switch (buttonView.getId()) {
            case R.id.updates:
                update.isChecked();
                break;
            case R.id.andG:
                andG.isChecked();
                break;
            case R.id.checkMe:
                checkmeOut.isChecked();
                break;

            default:
                break;
        }

    }

}
