package com.example.Setting;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.demo.R;

public class Deactive extends Fragment {

    public static final String TAG = "com.example.Setting.Deactive";

    ImageView actionLeft;
    TextView actionCenter, actionRight;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.deactive, container, false);

        actionLeft = (ImageView) v.findViewById(R.id.action_left);
        actionCenter = (TextView) v.findViewById(R.id.action_center);
        actionRight = (TextView) v.findViewById(R.id.action_right);
        actionCenter.setText(R.string.settings_account_deactivate);
        actionRight.setBackgroundDrawable(null);
        actionRight.setText("");

        actionLeft.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStack();

            }
        });

        return v;
    }
}
