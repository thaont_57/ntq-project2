package com.example.Setting;

import com.example.demo.R;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

public class Term extends Fragment {

    public static final String TAG = "com.example.Setting.Term";

    TextView actionCenter, actionRight;
    private ImageView actionLeft;
    WebView myWebview;
    String html = "http://jplang.tufs.ac.jp/vi/ka/1/1.html";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.term, container, false);

        myWebview = (WebView) v.findViewById(R.id.mywebview);
        actionCenter = (TextView) v.findViewById(R.id.action_center);
        actionRight = (TextView) v.findViewById(R.id.action_right);
        actionLeft = (ImageView) v.findViewById(R.id.action_left);
        actionCenter.setText(R.string.settings_terms_of_service);
        actionRight.setBackgroundDrawable(null);
        actionRight.setText("");

        myWebview.loadUrl(html);
        myWebview.getSettings().setBuiltInZoomControls(true);
        myWebview.getSettings().setSupportZoom(true);
        //myWebview.setWebViewClient(client); ko hien browses cua may

        actionLeft.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStack();

            }
        });

        return v;
    }
}
