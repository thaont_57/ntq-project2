package com.example.Auction;

import java.util.ArrayList;

import com.example.demo.R;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ListView;

public class ListAuction extends Fragment {
    public static final String TAG = "com.example.Auction.ListAuction";

    ArrayList<ItemID> itemID;
    GridView grid;
    AdapterListAuction myAdapterList;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.list_auction, container, false);
        ListView listview = (ListView) v.findViewById(R.id.listAuction);
        if (itemID == null) {
            itemID = new ArrayList<>();
            for (int i = 0; i < 5; i++) {
                ItemID data = new ItemID(i);
                itemID.add(data);
            }
        }
        //	Toast.makeText(getActivity(), itemID.get(0).getId()+"", Toast.LENGTH_SHORT).show();
        for (int i = 0; i < 4; i++) {
            if (i == 0) {
                myAdapterList = new AdapterListAuction(getActivity(), R.layout.grid_auction, itemID);
                listview.setAdapter(myAdapterList);
            } else if (i == 1) {
                myAdapterList = new AdapterListAuction(getActivity(), R.layout.grid_medium, itemID);
                listview.setAdapter(myAdapterList);
            } else {
                myAdapterList = new AdapterListAuction(getActivity(), R.layout.grid_small, itemID);
                listview.setAdapter(myAdapterList);
            }
        }

        return v;
    }

    public class AdapterListAuction extends ArrayAdapter<ItemID> {
        private ArrayList<ItemID> itemList;
        private LayoutInflater vi;
        public AdapterGrid mAdapter;
        int Resource;

        public AdapterListAuction(Context context, int textViewResourceId, ArrayList<ItemID> objects) {
            super(context, textViewResourceId, objects);
            vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            Resource = textViewResourceId;
            itemList = objects;
        }

        @SuppressLint("InflateParams")
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View v = convertView;
            // int type = (int) getItemId(position);
            ArrayList<ItemListAuction> item = new ArrayList<>();
            switch (itemList.get(position).getId()) {
                case 0:
                    v = vi.inflate(R.layout.grid_auction, null);
                    grid = (GridView) v.findViewById(R.id.grid);
                    for (int i = 0; i < 2; i++) {
                        ItemListAuction data = new ItemListAuction(R.drawable.default_male_bg, R.drawable.ic_status_online,
                                "12h remaining");
                        item.add(data);
                    }
                    mAdapter = new AdapterGrid(getActivity(), R.layout.item_auction, item);
                    grid.setAdapter(mAdapter);
                    break;
                case 1:
                    v = vi.inflate(R.layout.grid_medium, null);
                    grid = (GridView) v.findViewById(R.id.grid);
                    for (int i = 0; i < 4; i++) {
                        ItemListAuction data = new ItemListAuction(R.drawable.default_male_bg, R.drawable.ic_status_online,
                                "12h");
                        item.add(data);
                    }
                    mAdapter = new AdapterGrid(getActivity(), R.layout.item_auction_small, item);
                    grid.setAdapter(mAdapter);
                    break;
                case 2:
                case 3:
                case 4:
                    v = vi.inflate(R.layout.grid_small, null);
                    grid = (GridView) v.findViewById(R.id.grid);
                    for (int i = 0; i < 5; i++) {
                        ItemListAuction data = new ItemListAuction(R.drawable.default_male_bg, R.drawable.ic_status_online,
                                "12h");
                        item.add(data);
                    }
                    mAdapter = new AdapterGrid(getActivity(), R.layout.item_auction_small, item);
                    grid.setAdapter(mAdapter);
                    break;

                default:
                    break;
            }
            return v;
        }

    }
}
