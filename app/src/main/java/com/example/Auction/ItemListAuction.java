package com.example.Auction;

public class ItemListAuction {
    int Image;
    int State;
    String Time;

    public ItemListAuction(int image, int state, String time) {
        Image = image;
        State = state;
        Time = time;
    }

    public int getImage() {
        return Image;
    }

    public int getState() {
        return State;
    }

    public String getTime() {
        return Time;
    }

}
