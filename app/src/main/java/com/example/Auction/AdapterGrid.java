package com.example.Auction;

import java.util.ArrayList;

import com.example.demo.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class AdapterGrid extends ArrayAdapter<ItemListAuction> {
    private ArrayList<ItemListAuction> itemList;
    private LayoutInflater vi;
    ViewHolder holder;
    int Resource;

    public AdapterGrid(Context context, int textViewResourceId, ArrayList<ItemListAuction> objects) {
        super(context, textViewResourceId, objects);
        vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        Resource = textViewResourceId;
        itemList = objects;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            holder = new ViewHolder();
            v = vi.inflate(Resource, null);
            holder.image = (ImageView) v.findViewById(R.id.image);
            holder.state = (ImageView) v.findViewById(R.id.state);
            holder.time = (TextView) v.findViewById(R.id.time);
            v.setTag(holder);
        } else {
            holder = (ViewHolder) v.getTag();
        }

        holder.image.setImageResource(itemList.get(position).getImage());
        holder.state.setImageResource(itemList.get(position).getState());
        holder.time.setText(itemList.get(position).getTime());

        return v;
    }

    static class ViewHolder {
        ImageView image;
        ImageView state;
        TextView time;
    }

}
