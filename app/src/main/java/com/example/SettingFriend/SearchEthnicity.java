package com.example.SettingFriend;

import android.app.ListActivity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ListView;
import android.widget.TextView;

import com.example.demo.Login;
import com.example.demo.R;

public class SearchEthnicity extends ListActivity {
    public static final String KEY_ETHNICITY = "ethnicity";

    SharedPreferences shareData;
    SharedPreferences.Editor editor;
    String[] Ethnicity = new String[9];
    AdapterEthnicity mAdapter;
    ListView list;
    public static CheckBox checkAll;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_ethnicity);
        TextView actionLeft = (TextView) findViewById(R.id.action_left);
        TextView actionCenter = (TextView) findViewById(R.id.action_center);
        TextView actionRight = (TextView) findViewById(R.id.action_right);
        checkAll = (CheckBox) findViewById(R.id.checkall);

        actionLeft.setText(R.string.common_cancel);
        actionCenter.setText(R.string.ethnicity_title);
        actionRight.setText(R.string.sliding_menu_right_top_txt_done);

        list = getListView();
        list.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        list.setTextFilterEnabled(true);

        shareData = getSharedPreferences(Login.APP_NAME, Context.MODE_PRIVATE);

        Ethnicity = getResources().getStringArray(R.array.ethnicity);
        mAdapter = new AdapterEthnicity(this, R.layout.item_checkbox, Ethnicity);
        list.setAdapter(mAdapter);
        load();

        AdapterEthnicity.select = "";

        checkAll.setOnCheckedChangeListener(new OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (buttonView.isChecked()) {
                    for (int i = 0; i < 9; i++)
                        list.setItemChecked(i, true);
                } else {
                    for (int i = 0; i < 9; i++)
                        list.setItemChecked(i, false);
                }

            }
        });
        actionRight.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                save();
                onBackPressed();

            }
        });

        actionLeft.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                onBackPressed();

            }
        });

    }

    public void load() {
        String textEthnicity = shareData.getString(KEY_ETHNICITY, "All");
        if (textEthnicity.equals("All")) {
            checkAll.setChecked(true);
            for (int j = 0; j < 9; j++)
                list.setItemChecked(j, true);
        } else
            for (int i = 0; i < textEthnicity.length(); i++) {
                for (int j = 0; j < 9; j++) {
                    String subStr = Ethnicity[j];
                    int check = textEthnicity.indexOf(subStr);
                    if (check != -1) {
                        list.setItemChecked(j, true);
                    }
                }
            }

    }

    private void save() {
        editor = shareData.edit();
        editor.putString(KEY_ETHNICITY, AdapterEthnicity.select).commit();

    }

}
