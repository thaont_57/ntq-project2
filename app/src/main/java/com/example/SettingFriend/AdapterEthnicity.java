package com.example.SettingFriend;

import com.example.demo.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;

public class AdapterEthnicity extends ArrayAdapter<String> {
    String[] itemList;
    int Resource;
    LayoutInflater vi;
    ViewHolder holder;
    int count = 0;
    public static String select = "";

    public AdapterEthnicity(Context context, int textViewResourceId, String[] ethnicity) {
        super(context, textViewResourceId, ethnicity);
        // TODO Auto-generated constructor stub
        vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        Resource = textViewResourceId;
        itemList = ethnicity;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        //select ="";
        View v = convertView;
        if (v == null) {
            holder = new ViewHolder();
            v = vi.inflate(Resource, null);
            holder.myCheckbox = (CheckBox) v.findViewById(R.id.check);
            v.setTag(holder);
        } else
            holder = (ViewHolder) v.getTag();

        holder.myCheckbox.setText(itemList[position]);

        if (position == 8) {
            holder.myCheckbox.setBackgroundResource(R.drawable.boder_bottom);
        }

        if (position == 0) {
            holder.myCheckbox.setBackgroundResource(R.drawable.rectangle_no_border);
        }

        holder.myCheckbox.setOnCheckedChangeListener(new OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (buttonView.isChecked()) {
                    count++;
                    select += itemList[position] + ";\n";
                } else {
                    count--;
                    select = select.replace(itemList[position] + ";\n", "");
                    select = select.replace("All", "");
                    SearchEthnicity.checkAll.setChecked(false);
                }

                if (count == 9) {
                    select = "All";
                    SearchEthnicity.checkAll.setChecked(true);
                }
            }
        });
        this.notifyDataSetChanged();
        return v;
    }

    static class ViewHolder {
        CheckBox myCheckbox;
    }


}
