package com.example.SettingFriend;

import android.app.AlertDialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.example.demo.Login;
import com.example.demo.R;

public class SearchSetting extends DialogFragment
        implements NumberPicker.OnValueChangeListener, OnClickListener, OnSharedPreferenceChangeListener {

    public static final String KEY_AGEMIN = "age min";
    public static final String KEY_AGEMAX = "age max";
    public static final String KEY_SHOW = "show";
    public static final String KEY_INTEREST = "interest";
    public static final String KEY_WITHIN = "within";
    public static final String TAG = "com.example.SettingFriend.SearchSetting";
    public static final String woman = "凹";
    public static final String man = "凸";
    public static final String both = "凸 and 凹";

    LinearLayout AgesBetween;
    TextView agemin, agemax, Search_Ethnicity;
    ToggleButton Btn_show_woman, Btn_show_man, Btn_show_both, Btn_int_woman, Btn_int_man, Btn_int_both, near, city, state, country, world;

    public SharedPreferences shareData;
    public SharedPreferences.Editor editor;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.search_setting, container, false);

        agemin = (TextView) rootView.findViewById(R.id.agemin);
        agemax = (TextView) rootView.findViewById(R.id.agemax);
        AgesBetween = (LinearLayout) rootView.findViewById(R.id.ages_between);
        Search_Ethnicity = (TextView) rootView.findViewById(R.id.search_ethnicity);
        Btn_int_both = (ToggleButton) rootView.findViewById(R.id.btn_both_interest);
        Btn_int_man = (ToggleButton) rootView.findViewById(R.id.btn_man_interest);
        Btn_int_woman = (ToggleButton) rootView.findViewById(R.id.btn_woman_interest);
        Btn_show_both = (ToggleButton) rootView.findViewById(R.id.btn_both_show);
        Btn_show_man = (ToggleButton) rootView.findViewById(R.id.btn_man_show);
        Btn_show_woman = (ToggleButton) rootView.findViewById(R.id.btn_woman_show);
        near = (ToggleButton) rootView.findViewById(R.id.btn_near);
        city = (ToggleButton) rootView.findViewById(R.id.btn_city);
        state = (ToggleButton) rootView.findViewById(R.id.btn_state);
        country = (ToggleButton) rootView.findViewById(R.id.btn_country);
        world = (ToggleButton) rootView.findViewById(R.id.btn_world);

        shareData = getActivity().getSharedPreferences(Login.APP_NAME, Context.MODE_PRIVATE);
        editor = shareData.edit();
        load();

        AgesBetween.setOnClickListener(this);
        Search_Ethnicity.setOnClickListener(this);
        Btn_int_both.setOnClickListener(this);
        Btn_int_man.setOnClickListener(this);
        Btn_int_woman.setOnClickListener(this);
        Btn_show_both.setOnClickListener(this);
        Btn_show_man.setOnClickListener(this);
        Btn_show_woman.setOnClickListener(this);
        near.setOnClickListener(this);
        city.setOnClickListener(this);
        state.setOnClickListener(this);
        country.setOnClickListener(this);
        world.setOnClickListener(this);

        return rootView;
    }

    private void load() {
        String ageMin = shareData.getString(KEY_AGEMIN, "18");
        String ageMax = shareData.getString(KEY_AGEMAX, "120");
        String show = shareData.getString(KEY_SHOW, man);
        String interest = shareData.getString(KEY_INTEREST, both);
        String within = shareData.getString(KEY_WITHIN, "near");
        String theEthnicity = shareData.getString(SearchEthnicity.KEY_ETHNICITY, "All");
        // Toast.makeText(getActivity(), theEthnicity,
        // Toast.LENGTH_SHORT).show();

        //check save interest
        switch (interest) {
            case both:
                intBothCheck();
                break;
            case man:
                intManCheck();
                break;
            case woman:
                intWomanCheck();
                break;

            default:
                break;
        }

        //check save show
        switch (show) {
            case man:
                showManCheck();
                break;
            case woman:
                showWomanCheck();
                break;
            case both:
                showBothCheck();
                break;
            default:
                break;
        }

        //check save within
        switch (within) {
            case "Near":
                nearCheck();
                break;
            case "City":
                cityCheck();
                break;
            case "World":
                worldCheck();
                break;
            case "State":
                stateCheck();
                break;
            case "Country":
                countryCheck();
                break;
            default:
                break;
        }

        agemin.setText(ageMin);
        agemax.setText(ageMax);
        Search_Ethnicity.setText(theEthnicity);
    }

    @Override
    public void onStart() {
        load();
        super.onStart();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ages_between:
                show();
                break;
            case R.id.search_ethnicity:
                Intent myIntent = new Intent(getActivity().getApplicationContext(), SearchEthnicity.class);
                startActivity(myIntent);

            case R.id.btn_both_interest:
                Btn_int_man.setChecked(false);
                Btn_int_woman.setChecked(false);
                intBothCheck();
                break;

            case R.id.btn_both_show:
                Btn_show_man.setChecked(false);
                Btn_show_woman.setChecked(false);
                showBothCheck();

                break;
            case R.id.btn_woman_interest:
                Btn_int_man.setChecked(false);
                Btn_int_both.setChecked(false);
                intWomanCheck();
                break;
            case R.id.btn_woman_show:
                Btn_show_both.setChecked(false);
                Btn_show_man.setChecked(false);
                showWomanCheck();
                break;
            case R.id.btn_man_interest:
                Btn_int_woman.setChecked(false);
                Btn_int_both.setChecked(false);
                intManCheck();
                break;
            case R.id.btn_man_show:
                Btn_show_both.setChecked(false);
                Btn_show_woman.setChecked(false);
                showManCheck();
                break;
            case R.id.btn_near:
                city.setChecked(false);
                world.setChecked(false);
                state.setChecked(false);
                country.setChecked(false);
                nearCheck();
                break;
            case R.id.btn_city:
                near.setChecked(false);
                world.setChecked(false);
                state.setChecked(false);
                country.setChecked(false);
                cityCheck();
                break;
            case R.id.btn_state:
                near.setChecked(false);
                city.setChecked(false);
                world.setChecked(false);
                country.setChecked(false);
                stateCheck();
                break;
            case R.id.btn_country:
                near.setChecked(false);
                city.setChecked(false);
                world.setChecked(false);
                state.setChecked(false);
                countryCheck();
                break;
            case R.id.btn_world:
                near.setChecked(false);
                city.setChecked(false);
                state.setChecked(false);
                country.setChecked(false);
                worldCheck();
                break;
            default:
                break;
        }

    }

    private void intManCheck() {
        editor.putString(KEY_INTEREST, Btn_int_man.getText().toString());
        editor.commit();
        Btn_int_man.setChecked(true);

    }

    private void showWomanCheck() {
        editor.putString(KEY_SHOW, Btn_show_woman.getText().toString());
        editor.commit();
        Btn_show_woman.setChecked(true);

    }

    private void intWomanCheck() {
        editor.putString(KEY_INTEREST, Btn_int_woman.getText().toString());
        editor.commit();
        Btn_int_woman.setChecked(true);

    }

    private void showBothCheck() {
        editor.putString(KEY_SHOW, Btn_show_both.getText().toString());
        editor.commit();
        Btn_show_both.setChecked(true);
    }

    private void intBothCheck() {
        editor.putString(KEY_INTEREST, Btn_int_both.getText().toString());
        //	Toast.makeText(getActivity(), Btn_int_both.getText().toString(), Toast.LENGTH_SHORT).show();
        editor.commit();
        Btn_int_both.setChecked(true);
    }

    private void worldCheck() {
        editor.putString(KEY_WITHIN, world.getText().toString());
        editor.commit();
        world.setChecked(true);
    }

    private void countryCheck() {
        editor.putString(KEY_WITHIN, country.getText().toString());
        editor.commit();
        country.setChecked(true);

    }

    private void stateCheck() {
        editor.putString(KEY_WITHIN, state.getText().toString());
        editor.commit();
        state.setChecked(true);

    }

    private void cityCheck() {
        editor.putString(KEY_WITHIN, city.getText().toString());
        editor.commit();
        city.setChecked(true);

    }

    private void nearCheck() {
        editor.putString(KEY_WITHIN, near.getText().toString());
        editor.commit();
        near.setChecked(true);

    }

    private void showManCheck() {
        editor.putString(KEY_WITHIN, Btn_show_man.getText().toString());
        editor.commit();
        Btn_show_man.setChecked(true);
    }

    @Override
    public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
        Log.i("value is", "" + newVal);

    }

    public void show() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View alertDialogView;
        alertDialogView = inflater.inflate(R.layout.numberpicker, null);
        alertDialog.setView(alertDialogView);
        final NumberPicker np1 = (NumberPicker) alertDialogView.findViewById(R.id.numberPicker1);
        final NumberPicker np2 = (NumberPicker) alertDialogView.findViewById(R.id.numberPicker2);
        np1.setMaxValue(120);
        np1.setMinValue(18);
        np1.setWrapSelectorWheel(false);
        np1.setOnValueChangedListener(this);
        np2.setMaxValue(120);
        np2.setMinValue(18);
        np2.setWrapSelectorWheel(false);
        np2.setOnValueChangedListener(this);
        alertDialog.setPositiveButton("ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (np1.getValue() >= np2.getValue()) {
                    Toast.makeText(getActivity().getApplicationContext(),
                            "The age first must lesser than the age second", Toast.LENGTH_SHORT).show();
                } else {
                    editor.putString(KEY_AGEMIN, np1.getValue() + "");
                    editor.putString(KEY_AGEMAX, np2.getValue() + "");
                    editor.commit();
                    load();
                    dialog.dismiss();
                }
                Toast.makeText(getActivity().getApplicationContext(), np1.getValue() + "\n" + np2.getValue(),
                        Toast.LENGTH_SHORT).show();
            }
        });
        alertDialog.setNeutralButton("cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        alertDialog.show();
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        load();

    }
}
