package com.example.Gifts;

import java.util.ArrayList;

import com.example.demo.R;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

public class All extends Fragment {
    public static final String TAG = "com.example.Gifts.All";

    private GridView gridView;
    ArrayList<ItemGiftGrid> mArray;
    int[] ImageId = {R.drawable.ic_add_backstage_picture, R.drawable.ic_add_backstage_picture,
            R.drawable.ic_add_backstage_picture,
            R.drawable.ic_add_backstage_picture,
            R.drawable.ic_add_backstage_picture,
            R.drawable.ic_add_backstage_picture,
            R.drawable.ic_add_backstage_picture,
            R.drawable.ic_add_backstage_picture,
            R.drawable.ic_add_backstage_picture,
            R.drawable.ic_add_backstage_picture,
            R.drawable.ic_add_backstage_picture,
            R.drawable.ic_add_backstage_picture,
            R.drawable.ic_add_backstage_picture
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.grid_all, container, false);
        gridView = (GridView) v.findViewById(R.id.gridViewGift);
        getViews();
        return v;
    }

    private void getViews() {
        mArray = new ArrayList<>();
        for (int i = 0; i < 13; i++) {
            ItemGiftGrid a = new ItemGiftGrid(ImageId[i]);
            mArray.add(a);
        }
        AdapterGridGifts mAdapter = new AdapterGridGifts(getActivity().getApplicationContext(), R.layout.item_gift_grid, mArray);
        gridView.setAdapter(mAdapter);

    }


}
