package com.example.Gifts;

import com.example.Page.Gifts;
import com.example.demo.MainActivity;
import com.example.demo.R;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

public class FragmentGift extends Fragment {
    public static final String TAG = "com.example.Gifts.FragmentGift";
    private MainActivity parent;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.frag_gift, container, false);
        TextView all = (TextView) v.findViewById(R.id.all);
        parent = (MainActivity) getActivity();
        all.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                FragmentManager manager = getFragmentManager();
                FragmentTransaction transation = manager.beginTransaction();
                All all_fra = new All();
                transation.replace(R.id.frame_gift, all_fra);
                transation.addToBackStack(null);
                transation.commit();
                //Toast.makeText(getActivity(), Gifts.text_name.getText().toString(), Toast.LENGTH_SHORT).show();
                parent.setTitle(R.string.settings_chat_and_notifications_notifications_push_chat_all);
                parent.setActionBarRight(MainActivity.ID_IMAGE_NAV_RIGHT, MainActivity.ID_TEXT_NULL);
                parent.setActionBarLeft(R.drawable.nav_btn_back, MainActivity.ID_TEXT_NULL);
            }
        });
        return v;

    }

}
