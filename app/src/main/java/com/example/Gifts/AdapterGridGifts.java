package com.example.Gifts;

import java.util.ArrayList;

import com.example.demo.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

public class AdapterGridGifts extends ArrayAdapter<ItemGiftGrid> {
    LayoutInflater vi;
    int Resource;
    ViewHolder holder;
    ArrayList<ItemGiftGrid> itemGrid;

    public AdapterGridGifts(Context context, int textViewResourceId, ArrayList<ItemGiftGrid> objects) {
        super(context, textViewResourceId, objects);
        vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        Resource = textViewResourceId;
        itemGrid = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            holder = new ViewHolder();
            v = vi.inflate(Resource, null);
            holder.image = (ImageView) v.findViewById(R.id.image_gift);
            v.setTag(holder);
        } else
            holder = (ViewHolder) v.getTag();
        holder.image.setImageResource(R.drawable.ic_launcher);
        return v;
    }

    static class ViewHolder {
        ImageView image;
    }

}
