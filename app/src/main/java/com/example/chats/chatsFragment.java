package com.example.chats;

import com.example.demo.R;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

public class chatsFragment extends Fragment {
    public static final String TAG = "com.example.chats.chatsFragment";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.chats_fragment, container, false);
        ListView listview = (ListView) v.findViewById(R.id.listchats);
        ShareData.setData();
        AdapterListChats myAdapterList = new AdapterListChats(getActivity(), R.layout.item_list_chat, ShareData.mArray);
        listview.setAdapter(myAdapterList);
        return v;
    }
}
