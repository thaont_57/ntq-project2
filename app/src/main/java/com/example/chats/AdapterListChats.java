package com.example.chats;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.demo.R;

import java.util.ArrayList;

public class AdapterListChats extends ArrayAdapter<ItemViewChat>{
	LayoutInflater vi;
	int Resource;
	ArrayList<ItemViewChat> itemList;
	ViewHolder holder;

	public AdapterListChats(Context context, int textViewResourceId, ArrayList<ItemViewChat> objects) {
		super(context, textViewResourceId, objects);
		vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		Resource = textViewResourceId;
		itemList = objects;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View v = convertView;
		if(v == null){
			holder = new ViewHolder();
			v = vi.inflate(Resource, null);
			holder.image = (ImageView) v.findViewById(R.id.avar);
			holder.name= (TextView) v.findViewById(R.id.name);
			holder.messageReceive = (TextView) v.findViewById(R.id.message_receive);
			holder.messageSend = (TextView) v.findViewById(R.id.message_send);
			holder.distance = (TextView) v.findViewById(R.id.distance);
			holder.time = (TextView) v.findViewById(R.id.time);
			holder.Delete = (TextView) v.findViewById(R.id.btn_delete);
			v.setTag(holder);
		}
		else{
			holder = (ViewHolder) v.getTag();
		}
		holder.image.setImageResource(itemList.get(position).getImage());
		holder.name.setText(itemList.get(position).getName());
		holder.distance.setText(itemList.get(position).getDistance()+"");
		holder.time.setText(itemList.get(position).getTime()+"");
		holder.Delete.setVisibility(View.GONE);
		if("".equals(itemList.get(position).getMessageSend())){
			holder.messageReceive.setText(itemList.get(position).getMessageReceive());
			holder.messageReceive.setVisibility(View.VISIBLE);
			holder.messageSend.setVisibility(View.GONE);
		}
		else{
			holder.messageSend.setText(itemList.get(position).getMessageSend());
			holder.messageSend.setVisibility(View.VISIBLE);
			holder.messageReceive.setVisibility(View.GONE);
		}
		return v;
	}
	
	static class ViewHolder{
		TextView name;
		ImageView image;
		TextView distance;
		TextView time;
		TextView messageSend;
		TextView messageReceive;
		TextView Delete;
	}
}
