package com.example.chats;

import com.example.demo.R;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

public class chatsDeleteFragment extends Fragment{
	private ListView listview;
	private AdapterListChatsDelete myAdapterList;
	public static final String TAG = "com.example.chats.chatsDeleteFragment";

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.chatsdelete_fragment, container, false);
		listview = (ListView) v.findViewById(R.id.listchats_delete);
		ShareData.setData();
		myAdapterList = new AdapterListChatsDelete(getActivity().getApplicationContext(), R.layout.item_list_chat, ShareData.mArray,getActivity());
		listview.setAdapter(myAdapterList);
		
		return v;
	}

}
