package com.example.chats;

import java.util.ArrayList;

import com.example.demo.R;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class AdapterListChatsDelete extends ArrayAdapter<ItemViewChat> {
    LayoutInflater vi;
    int Resource;
    ArrayList<ItemViewChat> itemList;
    ViewHolder holder;
    Context context;
    Activity activity;

    public AdapterListChatsDelete(Context context, int textViewResourceId, ArrayList<ItemViewChat> objects, Activity a) {
        super(context, textViewResourceId, objects);
        this.context = context;
        vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        Resource = textViewResourceId;
        itemList = objects;
        activity = a;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            holder = new ViewHolder();
            v = vi.inflate(Resource, null);
            holder.image = (ImageView) v.findViewById(R.id.avar);
            holder.name = (TextView) v.findViewById(R.id.name);
            holder.messageReceive = (TextView) v.findViewById(R.id.message_receive);
            holder.messageSend = (TextView) v.findViewById(R.id.message_send);
            holder.distance = (TextView) v.findViewById(R.id.distance);
            holder.time = (TextView) v.findViewById(R.id.time);
            holder.Delete = (TextView) v.findViewById(R.id.btn_delete);
            v.setTag(holder);
        } else {
            holder = (ViewHolder) v.getTag();
        }
        holder.image.setImageResource(itemList.get(position).getImage());
        holder.name.setText(itemList.get(position).getName());
        holder.distance.setText(itemList.get(position).getDistance() + "");
        holder.time.setText(itemList.get(position).getTime() + "");
        // holder.Delete.setVisibility(View.VISIBLE);\
        holder.Delete.setTag(position);
        if ("".equals(itemList.get(position).getMessageSend())) {
            holder.messageReceive.setText(itemList.get(position).getMessageReceive());
            holder.messageReceive.setVisibility(View.VISIBLE);
            holder.messageSend.setVisibility(View.GONE);
        } else {
            holder.messageSend.setText(itemList.get(position).getMessageSend());
            holder.messageSend.setVisibility(View.VISIBLE);
            holder.messageReceive.setVisibility(View.GONE);
        }

        holder.Delete.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
//			Toast.makeText(getContext(), "click" + itemList.size() + " " + index.intValue(), Toast.LENGTH_SHORT)
//						.show();
                showDialog(v);
            }

            private void showDialog(final View vv) {
                final AlertDialog.Builder d = new AlertDialog.Builder(activity);
                d.setTitle("Delete");
                d.setMessage("Are you sure delete this message?");
                @SuppressLint("InflateParams") View myView = activity.getLayoutInflater().inflate(R.layout.dialog, null);
                d.setView(myView);
                TextView ok = (TextView) myView.findViewById(R.id.ok);
                TextView cancel = (TextView) myView.findViewById(R.id.cancel);
                final AlertDialog alert = d.create();
                alert.show();
                ok.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Integer index = (Integer) vv.getTag();
                        itemList.remove(index.intValue());
                        notifyDataSetChanged();
                        alert.dismiss();
                    }
                });
                cancel.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alert.dismiss();
                    }
                });

            }
        });
        return v;
    }

    static class ViewHolder {
        TextView name;
        ImageView image;
        TextView distance;
        TextView time;
        TextView messageSend;
        TextView messageReceive;
        TextView Delete;
    }

}