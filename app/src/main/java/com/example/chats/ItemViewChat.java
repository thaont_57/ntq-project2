package com.example.chats;

public class ItemViewChat {
    String Name;
    int Image;
    int Distance;
    int Time;
    String MessageSend;
    String MessageReceive;

    public ItemViewChat(String name, int image, int distance, int time, String messageSend, String messageReceive) {
        Name = name;
        Image = image;
        Distance = distance;
        Time = time;
        MessageSend = messageSend;
        MessageReceive = messageReceive;
    }

    public String getName() {
        return Name;
    }

    public int getImage() {
        return Image;
    }

    public int getDistance() {
        return Distance;
    }

    public int getTime() {
        return Time;
    }

    public String getMessageSend() {
        return MessageSend;
    }

    public String getMessageReceive() {
        return MessageReceive;
    }

}
