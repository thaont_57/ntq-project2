package com.example.demo;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

public class FragmentPageView extends Fragment {

    ImageView imageView;
    int image_id;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_viewpage1, container, false);
        imageView = (ImageView) view.findViewById(R.id.imageView);
        imageView.setImageResource(image_id);
        return view;
    }

    public static FragmentPageView newInstance(int image_id) {
        FragmentPageView f = new FragmentPageView();
        f.image_id = image_id;
        return f;
    }

}
