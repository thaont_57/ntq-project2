package com.example.demo;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.Json.Register;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Calendar;

public class SignUp extends Activity {

    public static final String URL = "http://14.160.24.93:9119/";
    public static final String TAG = SignUp.class
            .getSimpleName();

    TextView myBirthday, myGender;
    EditText myName, myEmail, myPassword, mymPassword;
    int yr, month, day;
    String[] genderArray;
    Person person;
    //  SharedPreferences shareData;
    //  SharedPreferences.Editor editor;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_sign_up);

        ImageButton btnLeft = (ImageButton) findViewById(R.id.action_left);
        TextView title = (TextView) findViewById(R.id.action_center);
        TextView signUp = (TextView) findViewById(R.id.signUp);
        myBirthday = (TextView) findViewById(R.id.birthday);
        myGender = (TextView) findViewById(R.id.gender);
        myName = (EditText) findViewById(R.id.newName);
        myEmail = (EditText) findViewById(R.id.Email);
        myPassword = (EditText) findViewById(R.id.password);
        mymPassword = (EditText) findViewById(R.id.password_confirm);

        btnLeft.setBackgroundResource(R.drawable.nav_btn_back);
        btnLeft.setVisibility(View.VISIBLE);
        title.setText(R.string.signup_sign_up);

        person = new Person();
        person.setName(myName.getText().toString());
        person.setEmail(myEmail.getText().toString());
        person.setGender((myGender.getText().toString()));
        person.setPassword(myPassword.getText().toString());
        person.setBirthday(myBirthday.getText().toString());

        myBirthday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePicker();
                Toast.makeText(getApplication(), "ShowDialog", Toast.LENGTH_SHORT).show();
            }
        });

        myGender.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showSelectGender();
            }
        });

        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!validate()) {
                    Toast.makeText(getBaseContext(), "Enter some data!", Toast.LENGTH_LONG).show();
                    return;
                }
                if (!checkPass()) {
                    Toast.makeText(getBaseContext(), "Error password!", Toast.LENGTH_LONG).show();
                    return;
                }
                new HttpAsyncTask().execute(URL);
            }
        });


    }

    private boolean checkPass() {
        return myPassword.getText().toString().equals(mymPassword.getText().toString());
    }

    private boolean validate() {
        return !myName.getText().toString().trim().equals("") && !myEmail.getText().toString().trim().equals("") && !myPassword.getText().toString().trim().equals("") && !mymPassword.getText().toString().trim().equals("") && !myGender.getText().toString().trim().equals("") && !myBirthday.getText().toString().trim().equals("");
    }

    private class HttpAsyncTask extends AsyncTask<String, Void, String> {


        @Override
        protected String doInBackground(String... params) {
            return POST(params[0], person);
        }

        @Override
        protected void onPostExecute(String s) {
            Toast.makeText(getApplicationContext(), "Data sent", Toast.LENGTH_SHORT).show();
        }
    }

    public String POST(String url, final Person person) {

        final String[] result = new String[1];
        final JSONObject object = new JSONObject();
        final String api = "?api=register_version_2";

        try {
            object.put("user_name", person.getName());
            object.put("email", person.getEmail());
            object.put("pwd", person.getPassword());
            object.put("bir", "20051122");
            object.put("inters_in", 0);
            object.put("device_id", "android");
            object.put("notify_token", "sjdks");
            object.put("device_type", 0);
            object.put("login_time", "20120223123412");
            object.put("ivt_code", "gshd");
            object.put("original_pwd", "dhnas");
            object.put("gender", 0);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        final Register register = new Gson().fromJson(object.toString(), Register.class);
        String s=sendRequest(url,api+register.toString(),15000,15000);
        Log.d("response",s+api+register.toString());
//        api += "&" + register.toString();
//        sendRequest(url, api.toString(), 0, 8  );
//        Log.d("api", api.toString());
//        url +=api + register;
//        final String finalUrl = url;
//        StringRequest sr = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String request) {
//                Log.d("json", request);
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError volleyError) {
//                Log.d("error", volleyError.getMessage());
//            }
//
//        });
//        {
//            @Override
//            protected Map<String, String> getParams() {
//                Map<String, String> params = new HashMap<String, String>();
//                params.put("register_version_2", api.toString() + register.toString());
//                Log.d("api", api.toString() + register.toString());
//                result[0] = api.toString() + register.toString();
//                return params;
//            }
//
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                Map<String, String> params = new HashMap<String, String>();
//                params.put("Content-Type", "application/x-www-form-urlencoded");
//                return params;
//            }
//        };
//
//        queue.add(sr);

        return result[0];
    }

    private void showDatePicker() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle("Choose birthday");
        LayoutInflater inflater = getLayoutInflater();
        final View alertDialogView = inflater.inflate(R.layout.dialog_date_picker, null);
        final DatePicker datePicker = (DatePicker) alertDialogView.findViewById(R.id.datePicker);
        dialog.setView(alertDialogView);
        Calendar today = Calendar.getInstance();
        yr = today.get(Calendar.YEAR);
        month = today.get(Calendar.MONTH);
        day = today.get(Calendar.DAY_OF_MONTH);
        new DatePickerDialog(this, mDataSetListener, yr, month, day);

        dialog.setNegativeButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String Birthday = datePicker.getDayOfMonth() + "/" + (datePicker.getMonth() + 1) + "/" + datePicker.getYear();
                myBirthday.setText(Birthday);
            }
        });

        dialog.show();
    }

    private void showSelectGender() {
        final AlertDialog dialog = new AlertDialog.Builder(this).create();
        dialog.setTitle("Choose gender");
        LayoutInflater inflater = getLayoutInflater();
        final View alertDialogView = inflater.inflate(R.layout.dialog_edit_gender, null);
        dialog.setView(alertDialogView);
        ListView myLst = (ListView) alertDialogView.findViewById(R.id.listgender);
        myLst.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        myLst.setTextFilterEnabled(true);
        genderArray = getResources().getStringArray(R.array.gender);
        ArrayAdapter<String> item = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1,
                genderArray);
        myLst.setAdapter(item);
        myLst.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String itemList = genderArray[position];
                myGender.setText(itemList);
                dialog.dismiss();
            }

        });

        dialog.show();
    }

    private DatePickerDialog.OnDateSetListener mDataSetListener = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            yr = year;
            month = monthOfYear;
            day = dayOfMonth;
        }
    };


    private String sendRequest(String url,
                               String inputString, int timeoutConnect, int timeoutRead) {
        String outputData;
        StringBuilder postData = new StringBuilder();
        URL u;
        HttpURLConnection conn = null;
        OutputStream out;
        InputStreamReader isr;
        BufferedReader buf;
        try {
            u = new URL(url);
            conn = (HttpURLConnection) u.openConnection();
            conn.setConnectTimeout(timeoutConnect);
            conn.setReadTimeout(timeoutRead);


            // post method
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            // data to send
            postData.append(inputString);
            String encodedData = postData.toString();
            // send data by byte
            conn.setRequestProperty("Content-Language", "en-US");
            conn.setRequestProperty("Content-Type",
                    "application/x-www-form-urlencoded");

            conn.setRequestProperty("Content-Length",
                    Integer.toString(encodedData.getBytes().length));
            byte[] postDataByte = postData.toString().getBytes("UTF-8");
            out = conn.getOutputStream();
            out.write(postDataByte);

            // get data from server
            isr = new InputStreamReader(conn.getInputStream(), "UTF-8");
            buf = new BufferedReader(isr);
            // write
            outputData = buf.readLine();
            out.close();
            isr.close();
            buf.close();
            return outputData;
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (conn != null)
                conn.disconnect();
        }
        return "";
    }

}
