package com.example.demo;

import android.app.Fragment;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.Page.Auction;
import com.example.Page.EditProfile;
import com.example.Page.Gifts;
import com.example.Page.NotificationsMain;
import com.example.Page.Points;
import com.example.Page.SearchFriends;
import com.example.Page.settings;
import com.example.RecentReport.RecentDetail;
import com.example.chats.AdapterListChats;
import com.example.chats.ShareData;
import com.example.chats.chatsDeleteFragment;
import com.example.chats.chatsFragment;
import com.example.myinterface.onClickListenActionBar;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.jeremyfeinstein.slidingmenu.lib.app.SlidingActivity;

public class MainActivity extends SlidingActivity implements View.OnClickListener {

    public static final int ID_IMAGE_NAV_LEFT1 = R.drawable.nav_menu;
    public static final int ID_IMAGE_NAV_LEFT2 = R.drawable.nav_btn_back;
    public static final int ID_IMAGE_NAV_RIGHT = R.drawable.nav_message;
    public static final int ID_TEXT_NULL = R.string.null_text;

    private SlidingMenu slidingMenu;
    public ImageButton actionLeft;
    public TextView actionCenter, actionRightText, actionLeftText, btnRightChat, btnLeftChat, btnCenterChat;
    private FrameLayout btnLeft, btnRight;
    public RelativeLayout actionRight;
    public onClickListenActionBar listener;
    boolean check = true;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setBehindContentView(R.layout.recent_report);

        slidingMenu = getSlidingMenu();
        slidingMenu.setMode(SlidingMenu.LEFT_RIGHT);
        slidingMenu.setTouchModeAbove(SlidingMenu.TOUCHMODE_FULLSCREEN);
        slidingMenu.setSecondaryMenu(R.layout.chats);

        getViewMenu();
        getViewActionBar();
        getViewChatRight();

        if (savedInstanceState == null) {
            Fragment fragMain = new RecentDetail();
            getFragmentManager().beginTransaction().add(R.id.frame_main, fragMain, fragMain.getClass().getName()).commit();
        }

        listener = new onClickListenActionBar() {
            @Override
            public void onClickActionBarLeft() {

            }

            @Override
            public void onClickActionBarRight() {

            }
        };

    }

    private void getViewChatRight() {
        ListView listview = (ListView) findViewById(R.id.listchats);
        ShareData.setData();
        AdapterListChats myAdapterList = new AdapterListChats(getApplication(), R.layout.item_list_chat, ShareData.mArray);
        listview.setAdapter(myAdapterList);

        btnRightChat = (TextView) findViewById(R.id.btn_right_chat);
        btnLeftChat = (TextView) findViewById(R.id.btn_left_chat);
        btnCenterChat = (TextView) findViewById(R.id.btn_center_chat);

        btnRightChat.setOnClickListener(this);
    }

    private void getViewActionBar() {
        actionRightText = (TextView) findViewById(R.id.textRight);
        actionCenter = (TextView) findViewById(R.id.action_center);
        actionLeft = (ImageButton) findViewById(R.id.action_left);
        actionLeftText = (TextView) findViewById(R.id.textLeft);
        actionRight = (RelativeLayout) findViewById(R.id.action_right);
        btnLeft = (FrameLayout) findViewById(R.id.btn_left);
        btnRight = (FrameLayout) findViewById(R.id.btn_right);

        btnLeft.setOnClickListener(this);
        btnRight.setOnClickListener(this);

    }

    public void setTitle(int title) {
        actionCenter.setText(title);
    }

    public void setActionBarLeft(int imageBtn, int textLeft) {
        actionLeft.setBackgroundResource(imageBtn);
        actionLeftText.setText(textLeft);
        if (imageBtn != ID_IMAGE_NAV_LEFT1 && imageBtn != ID_IMAGE_NAV_LEFT2) {
            actionLeftText.setVisibility(View.VISIBLE);
            actionLeft.setVisibility(View.GONE);
        } else {
            actionLeftText.setVisibility(View.GONE);
            actionLeft.setVisibility(View.VISIBLE);
        }
    }

    public void setActionBarRight(int imageBtn, int textRight) {
        actionRightText.setText(textRight);
        if (imageBtn != ID_IMAGE_NAV_RIGHT && textRight == MainActivity.ID_TEXT_NULL) {
            actionRight.setVisibility(View.GONE);
            actionRightText.setVisibility(View.GONE);
        } else if (imageBtn != ID_IMAGE_NAV_RIGHT && textRight != MainActivity.ID_TEXT_NULL) {
            actionRight.setVisibility(View.GONE);
            actionRightText.setVisibility(View.VISIBLE);
            // Toast.makeText(getApplication(), "image invisible", Toast.LENGTH_SHORT).show();
        } else {
            actionRight.setVisibility(View.VISIBLE);
            actionRightText.setVisibility(View.GONE);
        }
    }

    public void getViewMenu() {
        findViewById(R.id.searchs_friends).setOnClickListener(this);
        findViewById(R.id.ckeck_timeline).setOnClickListener(this);
        findViewById(R.id.Auction).setOnClickListener(this);
        findViewById(R.id.shaketochat).setOnClickListener(this);
        findViewById(R.id.profile).setOnClickListener(this);
        findViewById(R.id.chats).setOnClickListener(this);
        findViewById(R.id.points).setOnClickListener(this);
        findViewById(R.id.settings).setOnClickListener(this);
        findViewById(R.id.notifications_main).setOnClickListener(this);
        findViewById(R.id.btn_recent_report).setOnClickListener(this);
        findViewById(R.id.gift).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.points:
                replaceFragment(new Points());
                break;
            case R.id.searchs_friends:
                replaceFragment(new SearchFriends());
                break;
            case R.id.profile:
                replaceFragment(new EditProfile());
                break;
            case R.id.btn_recent_report:
                replaceFragment(new RecentDetail());
                break;
            case R.id.settings:
                replaceFragment(new settings());
                break;
//            case R.id.chats:
//                replaceFragment(new Chats());
//                break;
            case R.id.notifications_main:
                replaceFragment(new NotificationsMain());
                break;
            case R.id.gift:
                replaceFragment(new Gifts());
                break;
            case R.id.Auction:
                replaceFragment(new Auction());
                break;
            case R.id.action_left:
                listener.onClickActionBarLeft();
                break;
            case R.id.action_right:
                listener.onClickActionBarRight();
                break;
            case R.id.btn_right_chat:
                checkBtn();
                break;
            default:
                break;
        }
    }

    private void checkBtn() {
        if (!check) {
            Fragment chats = new chatsFragment();
            getFragmentManager().beginTransaction().replace(R.id.frame_content, chats, chatsFragment.TAG).commit();
            btnLeftChat.setText(R.string.shake_to_chat_title);
            btnLeftChat.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_menu_shaketochat, 0, 0);
            btnCenterChat.setText(R.string.add_friends_title);
            btnCenterChat.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_menu_add_friends, 0, 0);
            btnRightChat.setText(R.string.sliding_menu_right_top_txt_edit);
            btnRightChat.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_menu_edit, 0, 0);
            check = true;
        } else {
            Fragment deleteChats = new chatsDeleteFragment();
            getFragmentManager().beginTransaction().replace(R.id.frame_content, deleteChats, chatsDeleteFragment.TAG).commit();
            btnLeftChat.setText(R.string.sliding_menu_right_top_txt_mark_readed);
            btnLeftChat.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_menu_mark_readed, 0, 0);
            btnCenterChat.setText(R.string.sliding_menu_right_top_txt_delete_all);
            btnCenterChat.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_menu_delete_all, 0, 0);
            btnRightChat.setText(R.string.sliding_menu_right_top_txt_done);
            btnRightChat.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_menu_edit, 0, 0);
            check = false;
        }
    }

    private void replaceFragment(Fragment frag) {
        getFragmentManager().beginTransaction().replace(R.id.frame_main, frag, frag.getClass().getName()).addToBackStack(frag.getClass().getName()).commit();
        slidingMenu.showContent();
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
    }
}
