package com.example.demo;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

public class Login extends Activity {


    public static final String APP_NAME = "Data andG";
    public static final String KEY_PASSWORD = "password";
    public static final String KEY_EMAIL = "email";
    public static final String EMAIL = "thao@gmail.com";
    public static final String PASSWORD = "12345";

    private enum ErrorCode {
        SUCCESS(0),
        UNKNOWN_ERROR(1),
        WRONG_DATA_FORMAT(2),
        EMAIL_NOT_FOUND(10),
        INCORRECT_PASSWORD(20),
        LOCKED_USER(81);
        private int value;

        private ErrorCode(int v) {
            value = v;
        }
    }

    EditText email, password;
    ImageButton actionLeft;
    TextView actionCenter, actionRight;
    SharedPreferences shareData;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);

        email = (EditText) findViewById(R.id.email);
        password = (EditText) findViewById(R.id.pass);
        final TextView login = (TextView) findViewById(R.id.login);
        actionLeft = (ImageButton) findViewById(R.id.action_left);
        actionCenter = (TextView) findViewById(R.id.action_center);
        actionRight = (TextView) findViewById(R.id.textRight);

        actionCenter.setText(R.string.app_name);
        actionLeft.setBackgroundResource(R.drawable.nav_btn_back);
        actionRight.setText(R.string.login_forgot);
        actionRight.setVisibility(View.VISIBLE);
        findViewById(R.id.action_right).setVisibility(View.GONE);

        shareData = getSharedPreferences(APP_NAME, Context.MODE_PRIVATE);

        login.setOnClickListener(new OnClickListener() {


            @Override
            public void onClick(View v) {
                String emailText = email.getText().toString();
                String passwordText = password.getText().toString();
                new CheckLogin(emailText, passwordText).execute();
            }
        });
    }

    private class CheckLogin extends AsyncTask<Void, Void, ErrorCode> {

        public String email;
        public String password;

        public CheckLogin(String emailText, String passwordText) {
            email = emailText;
            password = passwordText;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected ErrorCode doInBackground(Void... params) {
            String pass = shareData.getString(KEY_PASSWORD, PASSWORD);
            if (email.equals(EMAIL) && password.equals(pass)) {
                return ErrorCode.SUCCESS;
            } else {
                return ErrorCode.INCORRECT_PASSWORD;
            }
        }

        @Override
        protected void onPostExecute(ErrorCode code) {
            super.onPostExecute(code);
            if (code == ErrorCode.SUCCESS) {
                SharedPreferences.Editor edittor = shareData.edit();
                edittor.putString(KEY_EMAIL, Login.this.email.getText().toString());
                edittor.putString(KEY_PASSWORD, Login.this.password.getText().toString());
                edittor.commit();
                Toast.makeText(getApplicationContext(), "Save data", Toast.LENGTH_SHORT).show();
                Intent myIntent = new Intent(Login.this, MainActivity.class);
                startActivity(myIntent);
            }
            if (code == ErrorCode.INCORRECT_PASSWORD) {
                Toast.makeText(getApplicationContext(), "loi", Toast.LENGTH_SHORT).show();
                final AlertDialog.Builder myNotice = new AlertDialog.Builder(Login.this);
                myNotice.setTitle("Error login!");
                myNotice.setMessage("Email or password is wrong");
                myNotice.setPositiveButton("OK", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
                myNotice.show();
                Login.this.password.setText("");
            }
        }
    }

}
