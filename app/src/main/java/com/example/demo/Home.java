package com.example.demo;

import android.app.Activity;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import android.widget.Toast;

import com.example.AdapterListGrid.MyPageAdapter;

public class Home extends FragmentActivity {
    ViewPager myViewPage;
    MyPageAdapter adapter;
    TextView signUp;
    TextView Login;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home);
        signUp = (TextView) findViewById(R.id.signup);
        Login = (TextView) findViewById(R.id.login);

        myViewPage = (ViewPager) findViewById(R.id.viewpager);
        adapter = new MyPageAdapter(getSupportFragmentManager());
        myViewPage.setAdapter(adapter);

        if (isConnect()) {
            Toast.makeText(getApplicationContext(), "Connected!", Toast.LENGTH_SHORT).show();
            signUp.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    startActivity(new Intent(Home.this, SignUp.class));
                }
            });

            Login.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    startActivity(new Intent(Home.this, Login.class));
                }
            });

        } else {
            Toast.makeText(getApplicationContext(), "Not Connected!", Toast.LENGTH_SHORT).show();
        }
    }

    private boolean isConnect() {
        ConnectivityManager connectMang = (ConnectivityManager) getSystemService(Activity.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = connectMang.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnected();
    }
}
