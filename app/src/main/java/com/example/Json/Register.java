package com.example.Json;

/**
 * Created by thao on 26/08/2015.
 */
public class Register {
    public String user_name;
    public String email;
    public String pwd;
    public String bir;
    public String inters_in;
    public String device_id;
    public String notify_token;
    public String device_type;
    public String login_time;
    public String ivt_code;
    public String original_pwd;
    public String gender;

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getBir() {
        return bir;
    }

    public void setBir(String bir) {
        this.bir = bir;
    }

    public String getDevice_id() {
        return device_id;
    }

    public void setDevice_id(String device_id) {
        this.device_id = device_id;
    }

    public String getDevice_type() {
        return device_type;
    }

    public void setDevice_type(String device_type) {
        this.device_type = device_type;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getInters_in() {
        return inters_in;
    }

    public void setInters_in(String inters_in) {
        this.inters_in = inters_in;
    }

    public String getIvt_code() {
        return ivt_code;
    }

    public void setIvt_code(String ivt_code) {
        this.ivt_code = ivt_code;
    }

    public String getLogin_time() {
        return login_time;
    }

    public void setLogin_time(String login_time) {
        this.login_time = login_time;
    }

    public String getNotify_token() {
        return notify_token;
    }

    public void setNotify_token(String notify_token) {
        this.notify_token = notify_token;
    }

    public String getOriginal_pwd() {
        return original_pwd;
    }

    public void setOriginal_pwd(String original_pwd) {
        this.original_pwd = original_pwd;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    @Override
    public String toString() {
        return "&user_name=" + user_name +"&email=" + email +"&pwd=" + pwd +"&bir=" + bir + "&inters_in=" + inters_in + "&device_id=" + device_id +"&notify_token="
                +notify_token+"&device_type=" + device_type + "&login_time=" + login_time +"&ivt_code=" + ivt_code + "&original_pwd=" + original_pwd + "&gender=" + gender;
    }
}
