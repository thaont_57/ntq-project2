package com.example.myinterface;

public interface onClickListenActionBar {

    public void onClickActionBarLeft();
    public void onClickActionBarRight();
}
