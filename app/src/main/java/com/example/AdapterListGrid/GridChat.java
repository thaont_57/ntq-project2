package com.example.AdapterListGrid;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import com.example.demo.R;

import java.util.ArrayList;

public class GridChat extends Fragment {

    public static final String TAG = "com.example.demo.ListChat";

    private GridView gridview;
    public MyAdapterGridView myAdapterList;
    View rootView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.gridchat, container, false);
        gridview = (GridView) rootView.findViewById(R.id.gridView);
        getview();
        return rootView;
    }

    private void getview() {
        ArrayList<ItemView> myArray = new ArrayList<>();
        for (int i = 0; i < 30; i++) {
            ItemView thao = new ItemView(R.drawable.default_avata, "thao", 22, "nu", "online", 70, "kimkaka");
            myArray.add(thao);
        }
        myAdapterList = new MyAdapterGridView(getActivity().getApplicationContext(), R.layout.grid_view_item, myArray);
        gridview.setAdapter(myAdapterList);

    }
}
