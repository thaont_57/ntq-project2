package com.example.AdapterListGrid;

public class ItemView {
    int Image;
    String Name;
    int Age;
    String Sex;
    String State;
    int Distance;
    String Message;

    public ItemView(int image, String name, int age, String sex, String state, int distance, String message) {
        Name = name;
        Age = age;
        State = state;
        Sex = sex;
        Distance = distance;
        Message = message;
        Image = image;
    }

    public int getImage() {
        return Image;
    }

    public String getName() {
        return Name;
    }

    public int getAge() {
        return Age;
    }

    public String getSex() {
        return Sex;
    }

    public String getState() {
        return State;
    }

    public int getDistance() {
        return Distance;
    }

    public String getMessage() {
        return Message;
    }
}
