package com.example.AdapterListGrid;

import java.util.ArrayList;

import com.example.demo.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class MyAdapterGridView extends ArrayAdapter<ItemView> {
    ArrayList<ItemView> itemList;
    LayoutInflater vi;
    int Resource;
    ViewHolder holder;

    public MyAdapterGridView(Context context, int textViewResourceId, ArrayList<ItemView> objects) {
        super(context, textViewResourceId, objects);
        vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        Resource = textViewResourceId;
        itemList = objects;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;
        if (v == null) {
            holder = new ViewHolder();
            v = vi.inflate(Resource, null);
            holder.imageview = (ImageView) v.findViewById(R.id.image_chat);
            holder.nameview = (TextView) v.findViewById(R.id.name_chat);
            v.setTag(holder);
        } else {
            holder = (ViewHolder) v.getTag();
        }
        holder.imageview.setBackgroundResource(itemList.get(position).getImage());
        holder.nameview.setText(itemList.get(position).getName());
        return v;

    }

    static class ViewHolder {
        public ImageView imageview;
        public TextView nameview;
    }


}
