package com.example.AdapterListGrid;

import java.util.ArrayList;

import com.example.demo.R;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

public class ListChat extends Fragment {

    public static final String TAG = "com.example.demo.ListChat";
    private ListView listview;

    View rootView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.listchat, container, false);
        listview = (ListView) rootView.findViewById(R.id.listchat);
        getview();
        return rootView;
    }

    private void getview() {
        ArrayList<ItemView> myArray = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            ItemView thao = new ItemView(R.drawable.default_avata, "thao", 22, "nu", "online", 70, "kimkaka");
            myArray.add(thao);
        }
        MyAdapterListView myAdapterList = new MyAdapterListView(getActivity().getApplicationContext(), R.layout.list_view_item, myArray);
        listview.setAdapter(myAdapterList);

    }

}
