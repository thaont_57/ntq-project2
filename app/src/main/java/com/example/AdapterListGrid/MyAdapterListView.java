package com.example.AdapterListGrid;

import java.util.ArrayList;

import com.example.demo.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class MyAdapterListView extends ArrayAdapter<ItemView> {
    ArrayList<ItemView> itemList;
    LayoutInflater vi;
    int Resource;
    ViewHolder holder;

    public MyAdapterListView(Context context, int textViewResourceId, ArrayList<ItemView> objects) {
        super(context, textViewResourceId, objects);
        vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        Resource = textViewResourceId;
        itemList = objects;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;
        if (v == null) {
            holder = new ViewHolder();
            v = vi.inflate(Resource, null);
            holder.imageview = (ImageView) v.findViewById(R.id.image_chat);
            holder.nameview = (TextView) v.findViewById(R.id.name_chat);
            holder.message = (TextView) v.findViewById(R.id.message_chat);
            holder.sex = (TextView) v.findViewById(R.id.sex);
            holder.Age = (TextView) v.findViewById(R.id.age);
            holder.State = (TextView) v.findViewById(R.id.state);
            holder.Distance = (TextView) v.findViewById(R.id.distance);
            v.setTag(holder);
        } else {
            holder = (ViewHolder) v.getTag();
        }
        holder.imageview.setImageResource(itemList.get(position).getImage());
        holder.nameview.setText(itemList.get(position).getName());
        holder.message.setText(itemList.get(position).getMessage());
        holder.Age.setText(itemList.get(position).getAge() + "");
        holder.State.setText(itemList.get(position).getState());
        holder.Distance.setText(itemList.get(position).getDistance() + "");
        holder.sex.setText(itemList.get(position).getSex());

        return v;

    }

    static class ViewHolder {
        public ImageView imageview;
        public TextView nameview;
        public TextView message;
        public TextView Age;
        public TextView sex;
        public TextView Distance;
        public TextView State;

    }

}
