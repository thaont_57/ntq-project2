package com.example.AdapterListGrid;


import com.example.demo.FragmentPageView;
import com.example.demo.R;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class MyPageAdapter extends FragmentPagerAdapter {

    int[] images = new int[]{
            R.drawable.splash_intro1,
            R.drawable.splash_intro2,
            R.drawable.splash_intro3,
            R.drawable.splash_intro4
    };

    public MyPageAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        // TODO Auto-generated method stub
        return FragmentPageView.newInstance(images[position]);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return images.length;
    }

}
